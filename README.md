# Merkle Tally Tree implentation

Implements merkle tally tree as described in
[Blockchain Voting Techniques](http://bitcoinunlimited.net/blockchain_voting)
of Bitcoin Unlimited.

> Extend a normal merkle tree at each node N with vote tallies of the sum of all the votes in the subtree rooted at N. The node hashes must concatenate the child hashes as in a normal Merkle tree and the vote tallies to ensure that the tallies cannot be modified without forcing the higher Merkle node hashes to change. The merkle tree root therefore contains the election results.

