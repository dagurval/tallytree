use sha2::{Digest, Sha256};
use std::collections::VecDeque;

type VoteReference = [u8; 32];
type TallyList = Vec<u32>;
type NodeRef = Option<Box<Node>>;
type NodeHash = [u8; 32];

/**
 * Tally tree node.
 */
#[derive(Debug)]
pub struct Node {
    /**
     * Reference to the cast vote and the vote it cast.
     */
    vote: Option<(VoteReference, Vec<u32>)>,
    left: NodeRef,
    right: NodeRef,
    is_wrapper: bool,
}

pub fn has_one_vote(tally: &[u32]) -> bool {
    tally.iter().sum::<u32>() == 1
}

pub fn is_null_node(node: &NodeRef) -> bool {
    match node {
        Some(n) => n.right.is_none() && n.left.is_none() && n.vote.is_none(),
        None => false,
    }
}

fn build_level(mut queue: VecDeque<Node>) -> Vec<Node> {
    let mut nodes: Vec<Node> = vec![];
    loop {
        let left = queue.pop_front();
        if left.is_none() {
            return nodes;
        }
        let right = queue.pop_front();
        nodes.push(Node {
            vote: None,
            left: Some(Box::new(left.unwrap())),
            right: Some(Box::new(right.unwrap())),
            is_wrapper: false,
        });
    }
}

pub fn generate_tree(votes: Vec<(VoteReference, TallyList)>) -> NodeRef {
    if votes.is_empty() {
        return None;
    }

    let leafs = votes.into_iter().map(|v| Node {
        vote: Some(v),
        left: None,
        right: None,
        is_wrapper: false,
    });

    // Wrap the leaf to secure against the issue bitcoin was vulnerable to.
    let nodes: Vec<Node> = leafs
        .map(|l| Node {
            vote: None,
            left: Some(Box::new(l)),
            right: None,
            is_wrapper: true,
        })
        .collect();

    // TODO: Handle special case where only one node has been cast.

    let mut queue = VecDeque::from(nodes);
    loop {
        if queue.len() == 1 {
            return Some(Box::new(queue.pop_front().unwrap()));
        }
        if queue.len() % 2 != 0 {
            // Push a NULL node to balance level.
            queue.push_back(Node {
                vote: None,
                left: None,
                right: None,
                is_wrapper: false,
            });
            assert!(queue.len() % 2 == 0);
        }
        queue = VecDeque::from(build_level(queue));
    }
}

pub fn pretty_print_tally(node: &NodeRef, level: usize) {
    if is_null_node(node) {
        println!("{:indent$}Ø", "", indent = level * 4);
        return;
    }
    match node {
        Some(n) => {
            let prefix = match n.vote {
                Some((v, _)) => [v[0], v[1]],
                None => {
                    let h = hash(node).unwrap();
                    [h[0], h[1]]
                }
            };
            if n.is_wrapper {
                print!(
                    "{:indent$}{:x?}=>{:?} --> ",
                    "",
                    prefix,
                    tally(node).unwrap(),
                    indent = level * 4
                );
                pretty_print_tally(&n.left, 0);
                assert!(n.right.is_none());
                return;
            }

            pretty_print_tally(&n.right, level + 1);
            println!(
                "{:indent$}{:x?}=>{:?}",
                "",
                prefix,
                tally(node).unwrap(),
                indent = level * 4
            );
            pretty_print_tally(&n.left, level + 1);
        }
        None => {}
    }
}

/**
 * Find the depth of the tree.
 */
pub fn tree_depth(node: &NodeRef) -> usize {
    match node {
        Some(n) => 1 + tree_depth(&n.left),
        None => 0,
    }
}

/**
 * Tally all the votes in the tree.
 */
pub fn tally(node: &NodeRef) -> Option<TallyList> {
    if node.is_none() {
        return None;
    }
    if is_null_node(node) {
        return None;
    }
    let node = node.as_ref().unwrap();
    if let Some((_, tally)) = &node.vote {
        assert!(node.left.is_none());
        assert!(node.right.is_none());
        assert!(has_one_vote(tally));
        return Some(tally.to_vec());
    }
    if node.right.is_none() || is_null_node(&node.right) {
        return tally(&node.left);
    }

    let tally_left = tally(&node.left).unwrap();
    let tally_right = tally(&node.right).unwrap();
    assert!(tally_left.len() == tally_right.len());
    Some(
        tally_left
            .iter()
            .zip(tally_right.iter())
            .map(|(l, r)| l + r)
            .collect(),
    )
}

/**
 * Hash of the entire tree.
 */
pub fn hash(node: &NodeRef) -> Option<NodeHash> {
    if is_null_node(node) {
        return Some([0x00; 32]);
    }
    match node {
        Some(n) => {
            let mut hasher = Sha256::new();
            if let Some((vote_reference, _)) = n.vote {
                assert!(n.left.is_none());
                assert!(n.right.is_none());
                hasher.update(vote_reference);
            }
            tally(node)
                .unwrap()
                .iter()
                .map(|t| t.to_be_bytes())
                .for_each(|b| hasher.update(b));
            if let Some(h) = hash(&n.left) {
                hasher.update(h);
            }
            if let Some(h) = hash(&n.right) {
                assert!(n.left.is_some());
                hasher.update(h);
            }
            Some(
                hasher
                    .finalize()
                    .as_slice()
                    .try_into()
                    .expect("Expected hash to be 32 bytes"),
            )
        }
        None => None,
    }
}

#[cfg(test)]
mod tests {
    use crate::*;

    #[test]
    fn pretty_print() {
        let head = generate_tree(vec![
            ([0x11; 32], vec![1, 0, 0]),
            ([0x22; 32], vec![0, 0, 1]),
            ([0x33; 32], vec![0, 0, 1]),
            ([0x44; 32], vec![0, 0, 1]),
            ([0x55; 32], vec![0, 0, 1]),
        ]);
        pretty_print_tally(&head, 0);
        assert_eq!(1, 1);
    }
}
